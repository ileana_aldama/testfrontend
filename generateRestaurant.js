var faker = require('faker');
faker.locale = 'es_MX';

var lat = 19.4328369;
var lng = -99.1337683;

function makeRestaurant() {
  return {
    id: faker.random.uuid(),
    rating: faker.random.number() % 5,
    name: faker.company.companyName(),
    contact: {
     site: faker.internet.url(),
     email: faker.internet.email(),
     phone: faker.phone.phoneNumber()
    },
    address: {
      street: faker.address.streetAddress(),
      city: faker.address.city(),
      state: faker.address.state(),
      location: {
        lat: lat + Math.random()/100,
        lng: lng + Math.random()/100
      }
    }
  };
}

exports.make = makeRestaurant;

