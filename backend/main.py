import json
import sys
from sqlalchemy import create_engine, text

engine = create_engine('sqlite:///./restaurants.sqlite', echo= True)
engine.execute(text("""
CREATE TABLE IF NOT EXISTS 
restaurantes(id TEXT PRIMARY KEY,
             rating INTEGER,
             name TEXT,
             site TEXT,
             email TEXT,
             phone TEXT,
             street TEXT,
             city TEXT,
             state TEXT,
             lat TEXT,
             lng TEXT)"""))

def insertdb(obj):
    engine.execute(text("""
    INSERT INTO restaurantes
    VALUES(:id,
           :rating,
           :name,
           :site,
           :email,
           :phone,
           :street,
           :city,
           :state,
           :lat,
           :lng)"""),
    id= obj['id'],
    rating= obj['rating'],
    name= obj['name'],
    site= obj['contact']['site'],
    email= obj['contact']['email'],
    phone= obj['contact']['phone'],
    street= obj['address']['street'],
    city= obj['address']['city'],
    state= obj['address']['state'],
    lat= obj['address']['location']['lat'],
    lng= obj['address']['location']['lng'])

def parsejson(file):
    data = open(file).read()
    jsonarray = json.loads(data)
    [insertdb(x) for x in jsonarray]

if __name__ == '__main__':
        parsejson(sys.argv[1])
